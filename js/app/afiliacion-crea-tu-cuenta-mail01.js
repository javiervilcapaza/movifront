
$.validator.setDefaults({
    submitHandler: function() {
        var campoRadio = $('input[name=keepmail]:checked').val();
        if(campoRadio == 'esteemail'){
            alert('Se seleciono la opción de seguir utilizando el correo');
            window.open('afiliacion-crea-tu-cuenta-mail03.html', '_self');
        }else{
            alert('Se seleciono la opción de actualizar el correo');
            window.open('afiliacion-crea-tu-cuenta-link-confirma.html', '_self');
        }
    }
});
$().ready(function() {
    // validamos antes de enviar
    $("#commentForm").validate({
        errorPlacement: function(error, element) {
            error.appendTo( element.parent().parent() );
        },
        rules: {
            keepmail: {
                required: true
            }
        },
        messages: {
            keepmail: {
                required: "debe seleccionar una opción"
            }
        }
    });
    submitHandler();
});