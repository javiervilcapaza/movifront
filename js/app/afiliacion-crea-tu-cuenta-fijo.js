$.validator.setDefaults({
    submitHandler: function() {
        alert("formulario válido!");
        window.open('afiliacion-gracias-por-crear-cuenta.html', '_self');
    }
});

$().ready(function() {

    $('#key').val('');

    $('.qtip01').each(function() {
        $(this).qtip({
            content: {
                text: $(this).next('.tooltiptext')
            }
        });
    });
    // validamos antes de enviar
    $("#commentForm").validate({
        errorPlacement: function(error, element) {
            if ( element.is(":checkbox") ) {
                error.appendTo( element.parent('.wrapper-check') );
            }
            else { // This is the default behavior
                error.appendTo( element.parent().next("#messageBox") );
            }
        },
        rules: {
            apswd: {
                required: true,
                minlength: 6
            },
            bpswd: {
                required: true,
                minlength: 6,
                equalTo: "#apswd"
            },
            key: {
                required: true
            }
        },
        messages: {
            apswd: {
                required: "Por favor llena este campo",
                minlength: "La contraseña no debe tener caracteres extraños y debe tener mínimo 6 caracteres"
            },
            bpswd: {
                required: "Por favor llena este campo",
                minlength: "La contraseña debe tener mínimo 6 caracteres",
                equalTo: "Ambas contraseñas deben coincidir"
            },
            key: {
                required: "Por favor llena este campo",
            }
        }
    });


    $('#passNumber').keypad({
        randomiseNumeric: true,
        showAnim: ''
    });
    submitHandler();
    togglePassword(document.getElementById('apswd'), document.getElementById('togglePasswordField') );
    togglePassword(document.getElementById('bpswd'), document.getElementById('togglePasswordFieldb') );
});


var togglePassword = function (passwordField, togglePasswordField) {
    passwordField.type = 'password';
    togglePasswordField.addEventListener('click', togglePasswordFieldClicked, false);
    togglePasswordField.style.display = 'inline';

    function togglePasswordFieldClicked(){
        var value = passwordField.value;
        if(passwordField.type == 'password') {
            passwordField.type = 'text';
        } else {
            passwordField.type = 'password';
        }
        passwordField.value = value;
    }
};
