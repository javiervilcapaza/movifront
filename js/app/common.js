function onlyNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

jQuery.validator.addMethod("emailVE", function (value, element, param) {
    if (this.optional(element)) {
        return true;
    }
    if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)) {
        return true;
    }
    return false;
}, "Ingresa un correo electrónico válido");

$(':text, :password, input[type="email"]').focus(function () {
    $(this).parent().parent().addClass("active");
});

$(':text, :password, input[type="email"]').blur(function () {
    if ($(this).hasClass("error")) {
    }
    else { // This is the default behavior
        $(this).parent().parent().removeClass("active");
    }
});

function submitHandler() {
    $('input').on('blur keyup click', function () {
        if (formIsValid()) {
            $('input[type=submit]').prop('disabled', false);
        } else {
            $('.customValidation').text('');
            $('input[type=submit]').prop('disabled', 'disabled');
        }
    });
}

function formIsValid(){
    return $("#commentForm").valid();
}

var setError = function (element, text) {
    var htmlError = '<span class="error customValidation">'+ text +'</span>'
    if (element.is("select")) {
        element.closest('.wrapper-select').find('#messageBox').html(htmlError);
    }
    if (element.is(":checkbox")) {
        element.closest('.wrapper-check').find('#messageBox').html(htmlError);
    }
    if (element.is("#fecha")) {
        element.next("#messageBox").find('.error').show();
        element.next("#messageBox").html(htmlError);
    }
    else {
        element.closest('.wrapper-form').find('#messageBox').html(htmlError);
    }
    return;
}

var cleanError = function (element) {
    try {
        if (element.is("select")) {
            element.closest('.wrapper-select').find('#messageBox .customValidation').text('');
        }
        if (element.is(":checkbox")) {
            element.closest('.wrapper-check').find('#messageBox .customValidation').text('');
        }
        if (element.is("#fecha")) {
            element.next("#messageBox").find('.customValidation').text('');
        }
        else {
            element.closest('.wrapper-form').find('#messageBox .customValidation').text('');
        }
        return;
    } catch (err) {
        console.log('Fallo al limpiar el mensaje de error');
    }
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var setDocumentNumber = function(element, parameter){
    var parameterValue = getUrlParameter(parameter);
    element.val(parameterValue);
    element.prop('disabled','disabled');
}