var countLoginIntent = 0;
var enabledPassword = false;
var dniRegistrado = '45611385';
var dniNoRegistrado = '45611386';
var ceNoRegistrado = '456113866';

var clave = 'javier';

$.validator.setDefaults({
    submitHandler: function () {

        if (($('#numdoc').val() == dniRegistrado && $('#pswd').val() == clave)) {
            alert('Acceso concedido');
        } else {
            countLoginIntent++;
            if($('#numdoc').val() == dniNoRegistrado || $('#numdoc').val() ==  ceNoRegistrado){
                alert('Existe DNI o CE pero no esta registrado, redirige a la ventana de afiliación');
                window.open('afiliacion-registro.html?doc='+$('#numdoc').val(), '_self');
            }
            if($('#numdoc').val() != dniRegistrado){
                setError($('#numdoc'), 'DNI o CE no existe en el sistema');
                captchaValidation()
            }else {
                cleanError($('#numdoc'));
            }
            if($('#pswd').val() != dniRegistrado){
                setError($('#pswd'), 'Contraseña incorrecta');
                captchaValidation()
            }else{
                cleanError($('#pswd'));
            }
            return;
        }
    }
});

var captchaValidation = function () {
    if (countLoginIntent > 2) {
        $('.recaptcha').show();
    };
}

// Carga inicial del documento
$().ready(function () {

    // crea tooltip
    $('.qtip01').each(function () {
        $(this).qtip({
            content: {
                text: $(this).next('.tooltiptext')
            }
        });
    });

    // validamos antes de enviar
    $("#commentForm").validate({
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().next("#messageBox"));
        },
        rules: {
            numdoc: {
                required: true,
                minlength: 8
            },
            pswd: {
                required: true,
                minlength: 6,

            },
        },
        messages: {
            numdoc: {
                required: "Número de Documento de Identidad vacío o incompleto",
                minlength: "Ingresa un documento de identidad de 8 o 9 dígitos"
            },
            pswd: {
                required: "Contraseña vacía",
                minlength: "La contraseña debe tener mínimo 6 caracteres",
                digits: "ingrese solo numeros"
            }
        }
    });

    submitHandler();

    try {
        var passwordField = document.getElementById('pswd');
        passwordField.type = 'text';
        passwordField.type = 'password';

        var togglePasswordField = document.getElementById('togglePasswordField');
        togglePasswordField.addEventListener('click', togglePasswordFieldClicked, false);
        togglePasswordField.style.display = 'inline';
    } catch (err) {
        console.log('Error al inicializar TOOGLE')
    }
});


function togglePasswordFieldClicked() {
    var passwordField = document.getElementById('pswd');
    var value = passwordField.value;
    if (passwordField.type == 'password') {
        passwordField.type = 'text';
    }
    else {
        passwordField.type = 'password';
    }
    passwordField.value = value;
}
