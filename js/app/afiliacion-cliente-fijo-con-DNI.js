var correoExistente = 'javier.vilcapaza@vasslatam.com';
var dniExistente = '45611386';
var fechaEmisionValida = '01012015';

$.validator.setDefaults({
    submitHandler: function() {
        var campoDNI = $('#numdoc').val();
        var campoFechaEmisio = $('#fecha').val();
        var campoCorreo = $('#aemail').val();

        if(campoDNI != dniExistente){
            setError($('#numdoc'), 'El número de documento no existe en el sistema');
            return;
        }else {
            cleanError($('#numdoc'));
        }

        if(campoFechaEmisio != fechaEmisionValida){
            console.log('asdf');
            setError($('#fecha'), 'La fecha de emisión del DNI no es válido');
            return;
        }else {
            cleanError($('#fecha'));
        }
        //alert('Formulario validado correctamente. se procede a validar correo.')

        if( campoCorreo == correoExistente){
            alert('La información de correo ya existe, redirige a crear la cuenta (FIJO)');
            window.open('afiliacion-crea-tu-cuenta-fijo.html', '_self');
        }else if(campoCorreo != correoExistente){
            alert('La información de correo es diferente, redirige a crea tu correo');
            window.open('afiliacion-crea-tu-cuenta-mail01.html', '_self');
        };
    }
});

function displayVals() {
    var dia = $( "#dia" ).val();
    var mes = $( "#mes" ).val();
    var anio = $( "#anio" ).val();
    $('input#fecha').val( dia + mes + anio );
    $('input#fecha').blur();
}

$( "#dia, #mes, #anio" ).change( displayVals );
displayVals();

$().ready(function() {
    $('.qtip01').each(function() {
        $(this).qtip({
            content: {
                text: $(this).next('.tooltiptext')
            }
        });
    });
    // validamos antes de enviar
    $("#commentForm").validate({
        errorPlacement: function(error, element) {
            console.log(error);
            if ( element.is("select") ) {
                error.appendTo( element.parent('.wrapper-select') );
            }
            if ( element.is(":checkbox") ) {
                error.appendTo( element.parent('.wrapper-check') );
            }
            if ( element.is("#fecha") ) {
                error.appendTo( element.next("#messageBox") );
            }
            else { // This is the default behavior
                error.appendTo( element.parent().next("#messageBox") );
            }
        },
        rules: {
            dia: {
                required: true
            },
            mes: {
                required: true
            },
            anio: {
                required: true
            },
            fecha: {
                required: true,
                minlength: 8,
                digits: true
            },
            numdoc: {
                //required: true,
                minlength: 8
            },
            aemail: {
                //required: true,
                email: true,
                emailVE:true
            },
            agree: {
                required: true
            }
        },
        messages: {
            fecha: {
                required: "Seleccione una fecha",
                minlength: "Seleccione una fecha completa",
                digits: "Sólo se permiten números"
            },
            numdoc: {
                //required: "Por favor llena este campo",
                minlength: "Ingresa un documento de identidad de 8 o 9 dígitos"
            },
            aemail: {
                //required: "Por favor llena este campo",
                email: "Ingresa un correo electrónico válido"
            },
            agree: {
                required: "debe aceptar los términos"
            }
        }
    });

    submitHandler();
    setDocumentNumber($('#numdoc'), 'dni');
});