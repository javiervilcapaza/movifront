var correoExistente = 'javier.vilcapaza@vasslatam.com';
var ceExistente = '456113866';

$.validator.setDefaults({
    submitHandler: function() {
        var campoCE = $('#numdoc').val();
        var campoCorreo = $('#aemail').val();

        if(campoCE != ceExistente){
            setError($('#numdoc'), 'El número de documento no existe en el sistema');
            return;
        }else {
            cleanError($('#numdoc'));
        }

        alert('Formulario validado correctamente. se procede a validar correo')

        if( campoCorreo == correoExistente){
            alert('La información de correo ya existe, redirige a crear la cuenta (FIJO)');
            window.open('afiliacion-crea-tu-cuenta-fijo.html', '_self');
        }else if(campoCorreo != correoExistente){
            alert('La información de correo es diferente, redirige a crea tu correo');
            window.open('afiliacion-crea-tu-cuenta-mail01.html', '_self');
        };
    }
});


$().ready(function() {
    $('.qtip01').each(function() {
        $(this).qtip({
            content: {
                text: $(this).next('.tooltiptext')
            }
        });
    });
    // validamos antes de enviar
    $("#commentForm").validate({
        errorPlacement: function(error, element) {
            if ( element.is(":checkbox") ) {
                error.appendTo( element.parent('.wrapper-check') );
            }
            else { // This is the default behavior
                error.appendTo( element.parent().next("#messageBox") );
            }
        },
        rules: {
            numdoc: {
                required: true,
                minlength: 9
            },
            aemail: {
                required: true,
                email: true,
                emailVE:true
            },
            agree: {
                required: true
            }
        },
        messages: {
            numdoc: {
                required: "Por favor llena este campo",
                minlength: "Ingresa un documento de identidad de 9 dígitos"
            },
            aemail: {
                required: "Por favor llena este campo",
                email: "Ingresa un correo electrónico válido"
            },
            agree: {
                required: "debe aceptar los términos"
            }
        }
    });
    submitHandler();
    setDocumentNumber($('#numdoc'), 'ce');
});