var fijaConDNI = '45611386';
var fijaConCE = '456113866';
var movilConDNI = '45611387';

$.validator.setDefaults({
    submitHandler: function () {
        var numdocValue = $('#numdoc').val();
        // Movil
        if (numdocValue.length == 8 && numdocValue == movilConDNI) {
            alert('Movil DNI');
            window.open('afiliacion-cliente-movil.html?dni=' + numdocValue, '_self');
            return;
        }
        // Fija con DNI
        if (numdocValue.length == 8 && numdocValue == fijaConDNI) {
            alert('Registro DNI');
            window.open('afiliacion-cliente-fijo-con-DNI.html?dni=' + numdocValue, '_self');
            return;
        }
        // Fija con CE
        if (numdocValue.length == 9 && numdocValue == fijaConCE) {
            alert('Registro CE');
            window.open('afiliacion-cliente-fijo-con-CE.html?ce=' + numdocValue, '_self');
            return;
        }
        alert('Usuario no existe, redirigiendo a la página web de movistar / Tienda online');
    }
});

// Carga inicial del documento
$().ready(function () {

    // crea tooltip
    $('.qtip01').each(function () {
        $(this).qtip({
            content: {
                text: $(this).next('.tooltiptext')
            }
        });
    });

    // validamos antes de enviar
    $("#commentForm").validate({
        errorPlacement: function (error, element) {
            error.appendTo(element.parent().next("#messageBox"));
        },
        rules: {
            numdoc: {
                required: true,
                minlength: 8
            }
        },
        messages: {
            numdoc: {
                required: "Número de Documento de Identidad vacío o incompleto",
                minlength: "Ingresa un documento de identidad de 8 o 9 dígitos"
            }
        }
    });

    submitHandler();
    setDocumentNumber($('#numdoc'), 'doc');
});